package tatica.com.br.PDFTest;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;


public class ConversorPadrao {
	
	void convertePadrao1() {
		String pathBase = ClassLoader.getSystemClassLoader().getResource("").getPath();
		String pathPadraoFatura = pathBase + "faturasPadrao1UmaFatura/";	
		File diretorio = new File(pathPadraoFatura);
		File fList[] = diretorio.listFiles();
		TransformaPNG transformador = new TransformaPNG();
		for(File file : fList) {
			if(file.toString().contains("Baixo")) {
				transformador.transformaImagem(file.toString());		
			}
			
		}
	}
	
	void convertePadrao2() {
		String pathBase = ClassLoader.getSystemClassLoader().getResource("").getPath();
		String pathPadraoFatura = pathBase + "faturasPadrao2DuasFaturaUmaEmpresa/";	
		File diretorio1 = new File(pathPadraoFatura);
		File fList1[] = diretorio1.listFiles();
		TransformaPNG transformador = new TransformaPNG();
		for(File file : fList1) {
			if((file.toString().contains("Baixo") || file.toString().contains("Cima")) && !file.toString().contains(".png")) {
				transformador.transformaImagem(file.toString());		
			}	
		}
		File diretorio2 = new File(pathPadraoFatura);
		File fList2[] = diretorio2.listFiles();
		for(File file : fList2) {
			String filePath = file.toString();
			if(filePath.contains("Baixo.pdf.png")) {
				String filePathBase = filePath.replaceAll(".pdfBaixo.pdf.png", "");
				String filePathTopo = filePathBase + ".pdfCima.pdffixed.pdfEmpresa.pdf.png";	
				try {
					 MixImagens mix = new MixImagens();
					 BufferedImage imagemCima = ImageIO.read(new File(filePathTopo));
					 BufferedImage imagemBaixo = ImageIO.read(new File(filePath));
					 BufferedImage joinedImg = mix.joinBufferedImage(imagemCima, imagemBaixo);
					 ImageIO.write(joinedImg, "png", new File(filePathBase+"UniaoEmpresaNome.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}		
			}	
		}
	}
	
	void convertePadrao3() {
		String pathBase = ClassLoader.getSystemClassLoader().getResource("").getPath();
		String pathPadraoFatura = pathBase + "faturasPadrao3DuasFaturasDuasEmpresas/";	
		File diretorio = new File(pathPadraoFatura);
		File fList[] = diretorio.listFiles();
		TransformaPNG transformador = new TransformaPNG();
		for(File file : fList) {
			if(file.toString().contains("Baixo") || file.toString().contains("Cima")) {
				transformador.transformaImagem(file.toString());		
			}	
		}
	}
	
	void criaPastaPadrao() {
		//percorre a pasta do padrao1
		String pathBase = ClassLoader.getSystemClassLoader().getResource("").getPath();
		String pathPadraoFatura = pathBase + "faturasPadrao1UmaFatura/";
		File diretorio = new File(pathPadraoFatura);
		File fList[] = diretorio.listFiles();
		for(File file : fList) {
			if(file.toString().contains(".png")) {
				String uc;
				String mesAnoRef;
				Tesseract tesseract = new Tesseract();
				uc = tesseract.leImagemRect(205, 130, 160, 30,file.toString());
				uc = uc.replaceAll("\\s+","");
				mesAnoRef = tesseract.leImagemRect(2890, 17, 140, 30,file.toString());
				mesAnoRef = mesAnoRef.replaceAll("\\s+","");
				mesAnoRef = mesAnoRef.replaceAll("\\D+","-");
				mesAnoRef = mesAnoRef.replaceAll("\\W+","-");
				Pattern padraoRegexMesAno = Pattern.compile("(\\d\\d\\s*)(?:\\D*\\d*)*(\\d\\d\\d\\d\\s*)"); 
				Matcher matcher = padraoRegexMesAno.matcher(mesAnoRef);
				String mes = new String();
				String ano = new String();
				matcher.find();
			    if (matcher.matches()) {
			    	mes = matcher.group(1).replaceAll("\\s+", "");
			    	ano = matcher.group(2).replaceAll("\\s+", "");
			    }
				String pathImagens = pathBase + "pastaImagens/";
				File diretorioFinal = new File(pathImagens + uc + "_" + mes + "-" + ano + ".png");
				try {
					FileUtils.copyFile(file, diretorioFinal);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}	
		}
			
		//percorre a pasta do padrao2
		String pathBase1 = ClassLoader.getSystemClassLoader().getResource("").getPath();
		String pathPadraoFatura1 = pathBase1 + "faturasPadrao2DuasFaturaUmaEmpresa/";
		File diretorio1 = new File(pathPadraoFatura1);
		File fList1[] = diretorio1.listFiles();
		for(File file : fList1) {
			if(file.toString().contains(".png") && !file.toString().contains("1.pdfBaixo.pdf") 
					&& !file.toString().contains("1.pdfCima.pdffixed.pdfEmpresa.pdf")) {
				String uc;
				String mesAnoRef;
				Tesseract tesseract = new Tesseract();
				uc = tesseract.leImagemRect(200, 129, 167, 32,file.toString());
				uc = uc.replaceAll("\\s+","");
				mesAnoRef = tesseract.leImagemRect(2885, 13, 142, 32,file.toString());
				mesAnoRef = mesAnoRef.replaceAll("\\s+","");
				mesAnoRef = mesAnoRef.replaceAll("\\D+","-");
				mesAnoRef = mesAnoRef.replaceAll("\\W+","-");
				Pattern padraoRegexMesAno = Pattern.compile("(\\d\\d\\s*)(?:\\D*\\d*)*(\\d\\d\\d\\d\\s*)"); 
				Matcher matcher = padraoRegexMesAno.matcher(mesAnoRef);
				String mes = new String();
				String ano = new String();
				matcher.find();
			    if (matcher.matches()) {
			    	mes = matcher.group(1).replaceAll("\\s+", "");
			    	ano = matcher.group(2).replaceAll("\\s+", "");
			    }
				String pathImagens = pathBase + "pastaImagens/";
				File diretorioFinal = new File(pathImagens + uc + "_" + mes + "-" + ano + ".png");
				try {
					FileUtils.copyFile(file, diretorioFinal);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}	
		}
		
		//percorre a pasta do padrao3
		String pathBase2 = ClassLoader.getSystemClassLoader().getResource("").getPath();
		String pathPadraoFatura2 = pathBase2 + "faturasPadrao3DuasFaturasDuasEmpresas/";
		File diretorio2 = new File(pathPadraoFatura2);
		File fList2[] = diretorio2.listFiles();
		for(File file : fList2) {
			if(file.toString().contains(".png")) {
				String uc;
				String mesAnoRef;
				Tesseract tesseract = new Tesseract();
				uc = tesseract.leImagemRect(200, 129, 167, 32,file.toString());
				uc = uc.replaceAll("\\s+","");
				mesAnoRef = tesseract.leImagemRect(2885, 13, 142, 32,file.toString());
				mesAnoRef = mesAnoRef.replaceAll("\\s+","");
				mesAnoRef = mesAnoRef.replaceAll("\\D+","-");
				mesAnoRef = mesAnoRef.replaceAll("\\W+","-");
				Pattern padraoRegexMesAno = Pattern.compile("(\\d\\d\\s*)(?:\\D*\\d*)*(\\d\\d\\d\\d\\s*)"); 
				Matcher matcher = padraoRegexMesAno.matcher(mesAnoRef);
				String mes = new String();
				String ano = new String();
				matcher.find();
			    if (matcher.matches()) {
			    	mes = matcher.group(1).replaceAll("\\s+", "");
			    	ano = matcher.group(2).replaceAll("\\s+", "");
			    }
				String pathImagens = pathBase + "pastaImagens/";
				File diretorioFinal = new File(pathImagens + uc + "_" + mes + "-" + ano + ".png");
				try {
					FileUtils.copyFile(file, diretorioFinal);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}	
		}
	
	}
	
}
