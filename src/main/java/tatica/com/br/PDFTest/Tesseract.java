package tatica.com.br.PDFTest;


import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;



import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract1;
import net.sourceforge.tess4j.TesseractException;

public class Tesseract {
	public static void main(String[] args) {
        
        //leImagemRect(2890, 17, 140, 30,"C:\\Users\\Tatica_Solutions\\Desktop\\1.pdfBaixo.pdf.png");
        //leImagemRect(205, 130, 100, 30,"C:\\Users\\Tatica_Solutions\\Desktop\\1.pdfBaixo.pdf.png");
		//teste corte UC
		File inicio = new File("C:\\Users\\Tatica_Solutions\\Desktop\\42803880_01-2018.png");
	    recorteImagens(1684, 406, 1490, 33 ,inicio,"C:\\Users\\Tatica_Solutions\\Desktop\\testecorteCOSIP1.png");
	    recorteImagens(1684, 441, 1490, 33 ,inicio,"C:\\Users\\Tatica_Solutions\\Desktop\\testecorteCOSIP2.png");
	    recorteImagens(1684, 479, 1490, 33 ,inicio,"C:\\Users\\Tatica_Solutions\\Desktop\\testecorteCOSIP3.png");
	    recorteImagens(1684, 516, 1490, 33 ,inicio,"C:\\Users\\Tatica_Solutions\\Desktop\\testecorteCOSIP4.png");
	    recorteImagens(1684, 554, 1490, 33 ,inicio,"C:\\Users\\Tatica_Solutions\\Desktop\\testecorteCOSIP5.png");
	    leImagem("C:\\Users\\Tatica_Solutions\\Desktop\\testecorteCOSIP3.png");
	    
		
		
		//teste corte Mes/Ano Referencia
		//File inicio1 = new File("C:\\Users\\Tatica_Solutions\\Desktop\\1.pdfBaixo.pdf.png");
		//recorteImagens(2890, 17, 140, 30,inicio1,"C:\\Users\\Tatica_Solutions\\Desktop\\testecorteAno.png");

    }

	static void recorteImagens(int x, int y, int width, int height, File file, String nome) {
		BufferedImage imagemCima;
		try {
			imagemCima = ImageIO.read(file);
	        BufferedImage subImage = imagemCima.getSubimage(x, y, width, height);
	        ImageIO.write(subImage, "png", new File(nome));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static void leImagem(String arquivo) {
		File imageFile = new File(arquivo);
        //ITesseract instance = new Tesseract();  // JNA Interface Mapping
        ITesseract instance = new Tesseract1(); // JNA Direct Mapping	

        try {
            String result = instance.doOCR(imageFile);
            System.out.println(result);
        } catch (TesseractException e) {
            System.err.println(e.getMessage());
        }
	}
	
	String leImagemRect(int x, int y, int width, int height, String path) {
		File imageFile = new File(path);
		String result = new String();
		ITesseract instance = new Tesseract1();
		Rectangle rect = new Rectangle(x, y, width, height);
        try {
			result = instance.doOCR(imageFile, rect);		
		} catch (TesseractException e) {
			e.printStackTrace();
		} 
        return result;
	}
}
