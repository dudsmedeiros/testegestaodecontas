package tatica.com.br.PDFTest;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;

public class GeraArquivosCELESC {

	public void separaPaginas(String arquivo) throws IOException {

		// ClassLoader.getSystemResource().get

		// ClassLoader.getSystemClassLoader().getResource(name);

		// Carrega o arquivo PDF desejado.
		String pathFaturasPDF = ClassLoader.getSystemClassLoader().getResource(arquivo).getPath();
		PDDocument doc = PDDocument.load(new File(pathFaturasPDF));
		System.out.printf("pathFaturasPDF: %s\n", pathFaturasPDF);
		System.out.printf("pages: %s\n", doc.getNumberOfPages());

		// Exclui a primeira pagina da relacao de faturas da CELESC, que corresponde a
		// capa.
		System.out.printf("remove first page\n");
		doc.removePage(0);
		System.out.printf("pages: %s\n", doc.getNumberOfPages());

		// Instancia a classe Splitter e divide o pdf carregado.
		System.out.printf("spliting pages...\n");
		Splitter splitter = new Splitter();
		List<PDDocument> docs = splitter.split(doc);

		// Cria um iterador na lista de paginas divididas.
		Iterator<PDDocument> iterator = docs.listIterator();

		// Salva cada pagina dividida como um arquivo individual.
		System.out.printf("saving pages ...\n");
		int i = 1;
		while (iterator.hasNext()) {
			PDDocument pd = iterator.next();

			// PDRectangle cropBox = pd.getDocumentCatalog().getPages().get(0).getCropBox();
			// PDRectangle cropBox = pd.getDocumentCatalog().getPages().getCropBox();
			// float upperRightY = cropBox.getUpperRightY();
			// float lowerLeftY = cropBox.getLowerLeftY();
			// cropBox.setLowerLeftY(upperRightY / 2);
			// pd.save("C:\\Tatica\\Algar\\Gestao_Contas\\CELESC\\Divididas\\"+
			// arquivoPDF.getName() + i++ +".pdf");
			// String pathFile = ClassLoader.getSystemResource("faturas/").getPath() + i++ +
			// ".pdf";
			String pathFile = ClassLoader.getSystemClassLoader().getResource("faturas/").getPath() + i++ + ".pdf";
			System.out.printf("pathFile: %s\n", pathFile);

			pd.save(ClassLoader.getSystemResource("faturas/").getPath() + i++ + ".pdf");
		}
		System.out.printf("docs.size(): %s\n", docs.size());
		doc.close();
		docs.forEach(d -> {
			try {
				d.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} );
	}
}