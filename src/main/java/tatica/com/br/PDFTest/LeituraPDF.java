package tatica.com.br.PDFTest;

import java.io.IOException;

import com.itextpdf.awt.geom.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.itextpdf.text.pdf.parser.RegionTextRenderFilter;
import com.itextpdf.text.pdf.parser.RenderFilter;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import com.itextpdf.text.pdf.parser.FilteredTextRenderListener;
import com.itextpdf.text.pdf.parser.LocationTextExtractionStrategy;

public class LeituraPDF {

	public String parsePdf(double x, double y, double width, double height, String pdf) throws IOException {
		PdfReader reader = new PdfReader(pdf);
		String resultado = new String();
		Rectangle rect = new Rectangle(x, y, width, height);
		RenderFilter filter = new RegionTextRenderFilter(rect);
		TextExtractionStrategy strategy;
		for (int i = 1; i <= reader.getNumberOfPages(); i++) {
			strategy = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), filter);
			// System.out.println(PdfTextExtractor.getTextFromPage(reader, i, strategy));
			resultado = PdfTextExtractor.getTextFromPage(reader, i, strategy);
		}
		reader.close();
		return resultado;
	}

}
