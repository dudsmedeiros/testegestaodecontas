package tatica.com.br.PDFTest;

import java.io.File;
import java.io.IOException;


import org.apache.commons.io.FileUtils;


public class TiposCorte {


	void CorteFaturaTipo3(int paginaAltura ,int paginaLargura ,int paginaX ,int referencialX ,int referencialW, 
							int referencialH, int compensadorCorte1 ,int compensadorCorte2 ,int numerodeCopias ){		
		//Faz o Primeiro Corte gerando a fatura de Cima( Ainda com cabeçalho) e a fatura de baixo com corte pronto
		//Salva a posição do topo da fatura de baixo para que o cabeçalho seja retirado da fatura de cima na próxima etapa
		int lowPos = 0;
		LeituraPDF testeLeitura = new LeituraPDF();
		String pathBase = ClassLoader.getSystemClassLoader().getResource("").getPath();
		String pathPadraoFatura = pathBase + "faturasPadrao3DuasFaturasDuasEmpresas/";	
		File diretorio = new File(pathPadraoFatura);
		File fList[] = diretorio.listFiles();
	
		for(File file : fList) {
			for (int y = 0; y <= paginaAltura; y++) {
				try {
					String caminho = file.toString();
					String comparacaoCorte = testeLeitura.parsePdf(referencialX, y, referencialW, referencialH,
						caminho);
					if (comparacaoCorte.contains("Empresa")) {
						CortePDF testeCorte = new CortePDF();
						lowPos = y + compensadorCorte2;
						testeCorte.cortaPDF("Cima", caminho , paginaX, (y + compensadorCorte1), paginaAltura, paginaLargura);
						testeCorte.cortaPDF("Baixo", caminho , paginaX, (y + compensadorCorte1), paginaAltura, -(paginaLargura));
						break;
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}		
		//Retira o cabeçalho da fatura de cima utilizando a variável lowPos(posição do topo da fatura de baixo) 
		for(File file : fList) {
			for (int y = paginaAltura; y > 0; y--) {
				try {
					String caminho = new String();
					String caminho2= new String();
					if(!(file.toString().contains("pdfCima")  || file.toString().contains("pdfBaixo"))) {
						caminho = file.toString();	
						caminho2 = file.toString() + "Cima.pdf";
						String comparacaoCorte = testeLeitura.parsePdf(referencialX, y, referencialW, referencialH,
								caminho);
						if (comparacaoCorte.contains("Empresa")) {
							CortePDF testeCorte = new CortePDF();
							testeCorte.cortaPDF("fixed",caminho2, paginaX, (y + compensadorCorte2), paginaAltura, -(y - lowPos));
							FileUtils.deleteQuietly(new File(caminho2));
							break;
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	void CorteFaturaTipo1(int paginaAltura ,int paginaLargura ,int paginaX ,int referencialX ,int referencialW, 
			int referencialH, int compensadorCorte1 ,int compensadorCorte2 ,int numerodeCopias ){	
		//Faz o Primeiro Corte gerando a unica fatura com corte pronto
		LeituraPDF testeLeitura = new LeituraPDF();
		String pathBase = ClassLoader.getSystemClassLoader().getResource("").getPath();
		String pathPadraoFatura = pathBase + "faturasPadrao1UmaFatura/";	
		File diretorio = new File(pathPadraoFatura);
		File fList[] = diretorio.listFiles();
	
		for(File file : fList) {
			for (int y = 0; y <= paginaAltura; y++) {
				try {
					String caminho = file.toString();
					String comparacaoCorte = testeLeitura.parsePdf(referencialX, y, referencialW, referencialH,
						caminho);
					if (comparacaoCorte.contains("Empresa")) {
						CortePDF testeCorte = new CortePDF();
						testeCorte.cortaPDF("Baixo", caminho , paginaX, (y + compensadorCorte1), paginaAltura, -(paginaLargura));
						break;
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
		
	}
	
	
	
	void CorteFaturaTipo2(int paginaAltura ,int paginaLargura ,int paginaX ,int referencialX ,int referencialW, 
			int referencialH, int compensadorCorte1 ,int compensadorCorte2 ,int numerodeCopias ){	
		//Faz o Primeiro Corte gerando a fatura de Cima( Ainda com cabeçalho) e a fatura de baixo com corte sem Empresa
		//Salva a posição do topo da fatura de baixo para que o cabeçalho seja retirado da fatura de cima na próxima etapa
		int lowPos = 0;
		LeituraPDF testeLeitura = new LeituraPDF();
		String pathBase = ClassLoader.getSystemClassLoader().getResource("").getPath();
		String pathPadraoFatura = pathBase + "faturasPadrao2DuasFaturaUmaEmpresa/";	
		File diretorio = new File(pathPadraoFatura);
		File fList[] = diretorio.listFiles();
		for(File file : fList) {
			for (int y = 0; y <= paginaAltura; y++) {
				try {
					String caminho = file.toString();
					String comparacaoCorte = testeLeitura.parsePdf(referencialX, y, referencialW, referencialH,
						caminho);
					if (comparacaoCorte.contains("Nome")) {
						CortePDF testeCorte = new CortePDF();
						lowPos = y + compensadorCorte1;
						testeCorte.cortaPDF("Cima", caminho , paginaX, (y + compensadorCorte1), paginaAltura, paginaLargura);
						testeCorte.cortaPDF("Baixo", caminho , paginaX, (y + compensadorCorte1), paginaAltura, -(paginaLargura));
						break;
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}	
		}
		//Retira o cabeçalho da fatura de cima utilizando a variável lowPos(posição do topo da fatura de baixo) 
		for(File file : fList) {
			for (int y = paginaAltura; y > 0; y--) {
				try {
					String caminho = new String();
					String caminho2= new String();
					if(!(file.toString().contains("pdfCima")  || file.toString().contains("pdfBaixo"))) {
						caminho = file.toString();	
						caminho2 = file.toString() + "Cima.pdf";
						String comparacaoCorte = testeLeitura.parsePdf(referencialX, y, referencialW, referencialH,
								caminho);
						if (comparacaoCorte.contains("Empresa")) {
							CortePDF testeCorte = new CortePDF();
							testeCorte.cortaPDF("fixed",caminho2, paginaX, (y + compensadorCorte2), paginaAltura, -(y - lowPos));
							FileUtils.deleteQuietly(new File(caminho2));
							break;
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		//Separa as informações da empresa
		for(File file : fList) {
			for (int y = paginaAltura; y > 0; y--) {
				try {
					String caminho = new String();
					String caminho2= new String();
					if(!(file.toString().contains("pdfCima")  || file.toString().contains("pdfBaixo"))) {
						caminho = file.toString();
						caminho2 = file.toString() + "Cima.pdffixed.pdf";
						String comparacaoCorte = testeLeitura.parsePdf(referencialX, y, referencialW, referencialH,
								caminho);
						if (comparacaoCorte.contains("Empresa")) {
							CortePDF testeCorte = new CortePDF();
							testeCorte.cortaPDF("Empresa",caminho2, paginaX, (y + compensadorCorte2), paginaAltura, -(compensadorCorte1));
							break;
						}							
					}
				}	
				catch (IOException e) {
					e.printStackTrace();
				}
			}		
		}
		
		
	}
	
}


