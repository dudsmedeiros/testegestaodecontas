package tatica.com.br.PDFTest;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class SeparadorFaturas {

	void SeparaPadrao(int referencialX , int referencialW , int referencialH,int paginaAltura ) {
	//Separa paginas e separa elas por padrões de faturas
			LeituraPDF testeLeitura = new LeituraPDF();
			int contadorEmpresas =0;
			int contadorNomes =0;
			int padraoFatura1 = 1;
			int padraoFatura2 = 1;
			int padraoFatura3 = 1;
			for(int x = 2 ; x <=74 ; x+=2) {
				contadorEmpresas =0;
				contadorNomes =0;
				//Percorre uma pagina
				for (int y = 0; y <= paginaAltura; y++) {
					try {
						String caminho = "faturas/" + String.valueOf(x) + ".pdf";
						String comparacaoCorte = testeLeitura.parsePdf(referencialX, y, referencialW, referencialH,
								ClassLoader.getSystemClassLoader().getResource(caminho).getPath());
						if (comparacaoCorte.contains("Empresa")) {
							contadorEmpresas +=1;
						}
						if (comparacaoCorte.contains("Nome")) {
							contadorNomes +=1;
						}
					
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				//Separando os casos
				//Caso faturasPadrao1UmaFatura
				if(contadorEmpresas == 7 && contadorNomes == 7) {
					String pathBase = ClassLoader.getSystemClassLoader().getResource("").getPath();	
					String caminho1 = pathBase + "/faturas/" + String.valueOf(x) + ".pdf";
					String caminho2 = pathBase + "/faturasPadrao1UmaFatura/" + String.valueOf(padraoFatura1) + ".pdf";
					padraoFatura1++;
					try {
						FileUtils.copyFile(new File(caminho1),new File(caminho2));
					} catch (Exception e) {
						e.printStackTrace();
					}	
				}
				//Caso faturasPadrao2DuasFaturaUmaEmpresa
				if(contadorEmpresas == 7 && contadorNomes == 14) {
					String pathBase = ClassLoader.getSystemClassLoader().getResource("").getPath();	
					String caminho1 = pathBase + "/faturas/" + String.valueOf(x) + ".pdf";
					String caminho2 = pathBase + "/faturasPadrao2DuasFaturaUmaEmpresa/" + String.valueOf(padraoFatura2) + ".pdf";
					padraoFatura2++;
					try {
						FileUtils.copyFile(new File(caminho1),new File(caminho2));
					} catch (Exception e) {
						e.printStackTrace();
					}	
				}
				//Caso faturasPadrao3DuasFaturasDuasEmpresas
				if(contadorEmpresas == 14 && contadorNomes == 14) {
					String pathBase = ClassLoader.getSystemClassLoader().getResource("").getPath();	
					String caminho1 = pathBase + "/faturas/" + String.valueOf(x) + ".pdf";
					String caminho2 = pathBase + "/faturasPadrao3DuasFaturasDuasEmpresas/" + String.valueOf(padraoFatura3) + ".pdf";
					padraoFatura3++;
					try {
						FileUtils.copyFile(new File(caminho1),new File(caminho2));
					} catch (Exception e) {
						e.printStackTrace();
					}	
				}
			}
	}		
}
