package tatica.com.br.PDFTest;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

public class TransformaPNG {
	
	void transformaImagem(String name) {
	PDDocument document;
	try {
		document = PDDocument.load(new File(name));
		PDFRenderer pdfRenderer;
		pdfRenderer = new PDFRenderer(document);
		for (int page = 0; page < document.getNumberOfPages(); ++page)
			{ 
			File outputfile = new File( name + ".png");
			BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);
			ImageIO.write(bim,"png", outputfile);
			}
		document.close();
		}
	catch (IOException e) {
		e.printStackTrace();
	}
	}
	
}
