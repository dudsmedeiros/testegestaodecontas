package tatica.com.br.PDFTest;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;

public class ProcessamentoCelesc {
	public static void main(String[] args) {
		
		Tesseract t = new Tesseract();
	//	//teste corte UC
	//	File inicio = new File("C:\\Users\\Tatica_Solutions\\Desktop\\1.pdfBaixo.pdf.png");
	//	t.recorteImagens(200, 129, 167, 32,inicio,"C:\\Users\\Tatica_Solutions\\Desktop\\testecorteUC.png");
	//					
	//	//teste corte Mes/Ano Referencia  
	//	File inicio1 = new File("C:\\Users\\Tatica_Solutions\\Desktop\\25273214_0172018.png");
	//	t.recorteImagens(2885, 13, 142, 32,inicio1,"C:\\Users\\Tatica_Solutions\\Desktop\\testecorteAno.png");
	//	
	//	System.out.println(t.leImagemRect(200, 129, 167, 32,"C:\\Users\\Tatica_Solutions\\Desktop\\25273214_0172018.png"));
	//	System.out.println(t.leImagemRect(2890, 17, 140, 30,"C:\\Users\\Tatica_Solutions\\Desktop\\25273214_0172018.png"));
		
	
	String pathBase = ClassLoader.getSystemClassLoader().getResource("").getPath();
	String pathImagens = pathBase + "pastaImagens/";
	File diretorio = new File(pathImagens);
	List<String> listaErros = new ArrayList<String>();
	File listImagens[] = diretorio.listFiles();
	
	//for(File imagem : listImagens) {
		    File imagem = new File(pathImagens + "23168464_03-2018.png");
			BufferedImage imagemPNG = null;
		    try {
				imagemPNG = ImageIO.read(imagem);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String faturaURL = imagem.getPath() ;
			//Valores mapeados: UC
			String uC = t.leImagemRect(200, 125, 172, 33,faturaURL);
			uC = uC.replaceAll("\\s+","");
			uC = uC.replaceAll("\\D+","-");
			
			// Mes Ano Referencia:
			String mesAnoRef = t.leImagemRect(2880, 15, 150, 35,faturaURL);
			mesAnoRef = mesAnoRef.replaceAll("\\s+","");
			mesAnoRef = mesAnoRef.replaceAll("\\D+","-");
			
			// Data da fatura
			String dataFatura = t.leImagemRect(2083, 222, 164, 25, faturaURL);
			dataFatura = dataFatura.replaceAll("\\s+","");
			dataFatura = dataFatura.replaceAll("\\D+","-");
			
			// Data de Vencimento
			String dataVencimento = t.leImagemRect(2134, 274, 156, 32, faturaURL);
			dataVencimento = dataVencimento.replaceAll("\\s+","");
			dataVencimento = dataVencimento.replaceAll("\\D+","-");
			
			// Nota Fiscal
			String notaFiscal = t.leImagemRect(1287, 277, 116, 30, faturaURL);
			notaFiscal = notaFiscal.replaceAll("\\s+","");
			notaFiscal = notaFiscal.replaceAll("\\D+","-");
			
			
			// Valor Total da Fatura
			String valorTotalFatura = t.leImagemRect(3069, 276, 199, 30, faturaURL);
			valorTotalFatura = valorTotalFatura.replaceAll("\\s+","");
			
			// Tributos:
			String baseCalcICMS = new String();
			String aliquotaICMS = new String();
			String valorICMS = new String();
			String valorCofins = new String();
			String valorPIS = new String(); 
			//Cria um Vetor com os retângulos relacionados aos tributos
			String tributos1 = t.leImagemRect(106, 406, 1477, 33, faturaURL);
			String tributos2 = t.leImagemRect(106, 441, 1477, 33, faturaURL);
			String tributos3 = t.leImagemRect(106, 479, 1477, 33, faturaURL);
			String tributos4 = t.leImagemRect(106, 516, 1477, 33, faturaURL);
			String vetRectTributos[] = new String [4];
			vetRectTributos[0] = (tributos1);
			vetRectTributos[1] = (tributos2);
			vetRectTributos[2] = (tributos3);
			vetRectTributos[3] = (tributos4);
			for(String trib : vetRectTributos) {
				trib = trib.replaceAll("\\s+"," ");
				String vetTributos[] = trib.split(" ");
				if(vetTributos[0].contains("ICMS")) {
					baseCalcICMS = vetTributos[1];
					aliquotaICMS = vetTributos[2].replaceAll("96", "%");
					valorICMS = vetTributos[3];
				}
				if(vetTributos[0].contains("COFINS")) {
					valorCofins = vetTributos[3];
				}
				if(vetTributos[0].contains("PIS")) {
					valorPIS = vetTributos[3];
				}
			}
			
			//Valores  Medidos
			String leituraAtual = new String();
			String medidos1 = t.leImagemRect(106, 612, 1477, 33, faturaURL);
			String medidos2 = new String();
			String medidos3 = new String();
			String medidos4 = new String();
			if(imagemPNG.getHeight() > 680) {
				medidos2 = t.leImagemRect(106, 647, 1477, 33, faturaURL);
			}
			if(imagemPNG.getHeight() > 716) {
				medidos3 = t.leImagemRect(106, 683, 1477, 33, faturaURL);
				medidos4 = t.leImagemRect(106, 718, 1477, 33, faturaURL);
			}
			String vetRectMedidos[] = new String [4];
			vetRectMedidos[0] = (medidos1);
			if(imagemPNG.getHeight() > 652) {
				vetRectMedidos[1] = (medidos2);
			}
			if(imagemPNG.getHeight() > 688) {
				vetRectMedidos[2] = (medidos3);
				vetRectMedidos[3] = (medidos4);
			}
			
			for(String med : vetRectMedidos) {
				med = med.replaceAll("\\s+"," ");
				String vetMedidos[] = med.split(" ");
				if(vetMedidos[0].contains("CON")) {
					leituraAtual = vetMedidos[4];
					break;
				}	
			}
			
			//Valores Faturados
			
			String consumo = new String();
			String taxaIluminacao = new String();
			String desconto = new String();
			String multa = new String();
			double multaValue = 0;
			double consumoValue = 0;
			String faturados1 = t.leImagemRect(1684, 406, 1490, 33, faturaURL);
			String faturados2 = t.leImagemRect(1684, 441, 1490, 33, faturaURL);
			String faturados3 = t.leImagemRect(1684, 479, 1490, 33, faturaURL);
			String faturados4 = t.leImagemRect(1684, 516, 1490, 33, faturaURL);
			String faturados5 = t.leImagemRect(1684, 554, 1490, 33, faturaURL);
			String faturados6 = t.leImagemRect(1684, 594, 1490, 33, faturaURL);
			String faturados7 = t.leImagemRect(1684, 629, 1490, 33, faturaURL);
			String faturados8 = t.leImagemRect(1684, 664, 1490, 33, faturaURL);
			String faturados9 = t.leImagemRect(1684, 699, 1490, 33, faturaURL);
			String faturados10 = t.leImagemRect(1684, 734, 1490, 33, faturaURL);
			String vetRectFaturados[] = new String [10];
			vetRectFaturados[0] = (faturados1);
			vetRectFaturados[1] = (faturados2);
			vetRectFaturados[2] = (faturados3);
			vetRectFaturados[3] = (faturados4);
			vetRectFaturados[4] = (faturados5);
			vetRectFaturados[5] = (faturados6);
			vetRectFaturados[6] = (faturados7);
			vetRectFaturados[7] = (faturados8);
			vetRectFaturados[8] = (faturados9);
			vetRectFaturados[9] = (faturados10);
			Pattern padraoRegex1 = Pattern.compile("(?:\\w+\\s+)+(\\d+\\.*\\d*\\s*,\\s*\\d+)\\s*"); 
			Pattern padraoRegex2 = Pattern.compile("(?:\\w+\\s+)(\\d+)\\s+.*");
			Pattern padraoRegex3 = Pattern.compile("(?:\\w+\\.*\\s+)+(?:\\w+\\s+)+(\\d+\\.*\\d*\\s*,\\s*\\d+)");
			Pattern padraoRegex4 = Pattern.compile("(?:\\w+\\s+)(?:\\w+\\s+)(?:\\w+\\s+)(\\d+)\\s+.*");
			Pattern padraoRegex5 = Pattern.compile("(?:\\w+\\s+)+(?:\\w+\\d+\\W+\\d+\\s)+(\\d*\\s*\\.*\\d+\\s*,\\s*\\d+)");
			for(String fat : vetRectFaturados) {
				fat = fat.replaceAll("\\s+"," ");
				fat = fat.trim();
				String vetFaturados[] = fat.split(" ");
				
				if(vetFaturados[0].contains("ADIC")) {
					continue;
				}
				
				if(vetFaturados[0].contains("MULTA") || vetFaturados[0].contains("CORRECAO") || vetFaturados[0].contains("JUROS")) {
					//Correções de erros possíveis la leitura
					fat = fat.replaceAll("l","1");
					System.out.println("regex: " + fat);
					fat = fat.replace(" ","");
					Matcher matcher = padraoRegex5.matcher(fat);
					matcher.find();
				    if (matcher.matches()) {
				    	String resultado = matcher.group(1).replaceAll("\\s+", "");
				    	resultado = resultado.replaceAll("O","0");
				    	multaValue = multaValue + Double.valueOf(resultado.replaceAll(",", "."));
				    }
					continue;
				}
				
				if(vetFaturados[0].contains("CUSTO")) {
					Matcher matcher = padraoRegex4.matcher(fat);
					matcher.find();
				    if (matcher.matches()) {
				    	consumoValue = consumoValue +  Double.valueOf(matcher.group(1));
				    }
					continue;
				}
				
				if(vetFaturados[0].contains("CONSUMO")) {
					Matcher matcher = padraoRegex2.matcher(fat);
					matcher.find();
				    if (matcher.matches()) {
				    	consumoValue = consumoValue +  Double.valueOf(matcher.group(1));
				    }
					continue;
				}
						
				if(vetFaturados[0].contains("COSIP")) {
					Matcher matcher = padraoRegex1.matcher(fat);
				    matcher.find();
				    if (matcher.matches()) {
				    	taxaIluminacao = matcher.group(1);
				    }
					continue;
				}
				if(vetFaturados[0].contains("DEVOLUCAO") || vetFaturados[0].contains("COMP")) {
					Matcher matcher = padraoRegex1.matcher(fat);
				    matcher.find();
				    if (matcher.matches()) {
				    	desconto = matcher.group(1);
				    }
					continue;
				}
				else {
					System.out.println("regex: " + fat);
					listaErros.add(uC + "-" + "Atributo não conhecido na fatura");
				}					
			}
			consumo = String.valueOf(consumoValue);
			consumo = consumo.replace(".", ",");
			DecimalFormat df = new DecimalFormat("#.##");
			multa = String.valueOf(df.format(multaValue));
			multa = multa.replace(".", ",");
			desconto = desconto.replaceAll(" ", "");
			taxaIluminacao = taxaIluminacao.replaceAll(" ", "");
			
			if(StringUtils.isBlank(taxaIluminacao)) {
				listaErros.add(uC + "-" + "Taxa Iluminação não encontrada");
			}
			
	
			System.out.printf("Nova Leitura: %s\n", imagem.getName());
			System.out.printf("UC: %s\n", uC);
			System.out.printf("Nota Fiscal: %s\n", notaFiscal);
			System.out.printf("Mes Ano Referencia: %s\n", mesAnoRef);
			System.out.printf("Data da fatura: %s\n", dataFatura);
			System.out.printf("Data de Vencimento: %s\n", dataVencimento);
			System.out.printf("Valor Total da Fatura: %s\n", valorTotalFatura);
			System.out.printf("Base Calculo ICMS: %s\n", baseCalcICMS);
			System.out.printf("Aliquota ICMS: %s\n", aliquotaICMS);
			System.out.printf("Valor ICMS: %s\n", valorICMS);
			System.out.printf("Valor CONFINS: %s\n", valorCofins);
			System.out.printf("Valor PIS: %s\n", valorPIS);
			System.out.printf("Leitura Atual: %s\n", leituraAtual);
			System.out.printf("Consumo: %s\n", consumo);
			System.out.printf("Multas:  %s\n", multa);
			System.out.printf("Desconto: %s\n", desconto);
			System.out.printf("Taxa de Iluminação: %s\n", taxaIluminacao);
			System.out.printf("\n\n\n");

		//}
	listaErros.forEach(erros -> System.out.println(erros));
	}
}