package tatica.com.br.PDFTest;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;


public class CortePDF {

	void cortaPDF( String name, String arquivo, float x, float y, float PDFwidht, float PDFheight) {
		PDDocument pdfCorte = null;
		try {
			String pathFaturasPDF = arquivo;
			pdfCorte = PDDocument.load(new File(pathFaturasPDF));
			PDPage pdfPage = pdfCorte.getPages().get(0);
			pdfPage.setCropBox(new PDRectangle(x, y, PDFwidht, PDFheight));
			pdfPage.setMediaBox(new PDRectangle(x, y, PDFwidht, PDFheight));
			pdfCorte.save(pathFaturasPDF + name +".pdf");
			//pdfCorte.save("E:\\Estudo\\PDFTest\\src\\main\\resources\\" + name +".pdf");
			pdfCorte.close();
		} catch (InvalidPasswordException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
