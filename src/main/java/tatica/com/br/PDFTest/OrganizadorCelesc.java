package tatica.com.br.PDFTest;


import java.io.IOException;


public class OrganizadorCelesc {
	public static void main(String[] args) {
			
		GeraArquivosCELESC separadorPaginas = new GeraArquivosCELESC();
		try {
			separadorPaginas.separaPaginas("faturas/2018-03-all.pdf");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		//Valores Utilizados para realizar os cortes e leituras
		int paginaAltura = 850;
		int paginaLargura = 270;
		int paginaX = 0;
		int referencialX = 17;
		int referencialW = 40;
		int referencialH = 7;
		int compensadorCorte1 = 17;
		int compensadorCorte2 = 11;
		int numerodeCopias = 74;

	    SeparadorFaturas separaFaturas = new SeparadorFaturas();
		separaFaturas.SeparaPadrao(referencialX, referencialW, referencialH, paginaAltura);
		
		TiposCorte cortePadrao1 = new TiposCorte();
		cortePadrao1.CorteFaturaTipo1(paginaAltura, paginaLargura, paginaX, referencialX, referencialW, 
				referencialH, compensadorCorte1, compensadorCorte2, numerodeCopias);
		
		TiposCorte cortePadrao2 = new TiposCorte();
		cortePadrao2.CorteFaturaTipo2(paginaAltura, paginaLargura, paginaX, referencialX, referencialW, 
				referencialH, compensadorCorte1, compensadorCorte2, numerodeCopias);
		
		TiposCorte cortePadrao3 = new TiposCorte();
		cortePadrao3.CorteFaturaTipo3(paginaAltura, paginaLargura, paginaX, referencialX, referencialW, 
				referencialH, compensadorCorte1, compensadorCorte2, numerodeCopias);
		
		ConversorPadrao conversorPadrao1 = new ConversorPadrao();
		conversorPadrao1.convertePadrao1();
		
		ConversorPadrao conversorPadrao2 = new ConversorPadrao();
		conversorPadrao2.convertePadrao2();
		
		ConversorPadrao conversorPadrao3 = new ConversorPadrao();
		conversorPadrao3.convertePadrao3();
        
		ConversorPadrao final1 = new ConversorPadrao();
		final1.criaPastaPadrao();
		
		
//		TransformaPNG teste = new TransformaPNG();
//		teste.transformaImagem("C:\\Users\\Tatica_Solutions\\Desktop\\1.pdfBaixo.pdf");

		}
}
