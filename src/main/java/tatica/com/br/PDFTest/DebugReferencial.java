package tatica.com.br.PDFTest;

import java.io.IOException;

public class DebugReferencial {

	public static void main(String[] args) {

		int referencialX = 17;
		int referencialW = 40;
		int referencialH = 7;
		int paginaAltura = 850;

		LeituraPDF testeLeitura = new LeituraPDF();
		for (int y = 0; y <= paginaAltura; y++) {
			try {
				String comparacaoCorte = testeLeitura.parsePdf(referencialX, y, referencialW, referencialH,
						"C:\\Users\\Tatica_Solutions\\Desktop\\PastaDebug\\celesc_pagina.pdf");
				System.out.println(String.valueOf(y) + " " +  comparacaoCorte);
				CortePDF testeCorte = new CortePDF();
				testeCorte.cortaPDF(String.valueOf(y) ,"C:\\Users\\Tatica_Solutions\\Desktop\\PastaDebug\\celesc_pagina.pdf", referencialX, y, referencialW, referencialH);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
